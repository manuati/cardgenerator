﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGameObject : MonoBehaviour
{
    private CardData cardData;
    private Text titleText;
    private Text typeText;
    private Text effectText;

    private void Start()
    {
        var canvas = this.GetComponentInChildren<Canvas>();
        var textFields = canvas.GetComponentsInChildren<Text>();
        foreach (Text text in textFields)
        {
            Debug.Log("TextField name: "+text.name);
            if (text.name == "Titulo") titleText = text;
            if (text.name == "Tipo") typeText = text;
            if (text.name == "Efecto") effectText = text;
        }
        Debug.Log("Fields loaded in CardGameObject");

    }

    public void SetCardData(CardData cardData)
    {
        this.cardData = cardData;
        titleText.text = this.cardData.name;
        typeText.text = this.cardData.type;
        effectText.text = this.cardData.effect;
    }

    public string GetCardName()
    {
        var cardName = cardData.name.ToLower() + "-" + cardData.version;

        return cardName;
    }
}
