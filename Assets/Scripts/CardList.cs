﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardList
{
    public string version;
    public CardData[] cards;
}
