﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CardData
{
    public string name;
    public string type;
    public string effect;
    public string version;

    public override string ToString()
    {
        return "CardData: Name: "+name;
    }
}
