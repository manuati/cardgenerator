﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ImageGenerator : MonoBehaviour
{
    public CardGameObject cardGameObject;
    public string FileName;
    private CardList cardList;

    public Canvas CardCanvas;
    public int Width;
    public int Height;
    private CanvasScreenshot screenShot;
    // Start is called before the first frame update

    public delegate void takePictureHandler(byte[] pngArray, string fileName);
    public static event takePictureHandler OnPictureTaken;

    private WaitForEndOfFrame frameEnd = new WaitForEndOfFrame();
    private WaitForSeconds waitForSeconds = new WaitForSeconds(1);

    [Range(0,1)]
    public float DelayBetweenScreenshots = 0.5f;
    private float _delay;
    private int _cardCounter = 0;

    void Start()
    {
        LoadCardDataFromFile(FileName);
        
        screenShot = this.GetComponent<CanvasScreenshot>();

        ImageGenerator.OnPictureTaken += receivePNGScreenShot;
        _delay = DelayBetweenScreenshots;
    }

    private void Update()
    {
        if (_cardCounter < cardList.cards.Length)
        {
            if (_delay <=0)
            {
                Debug.Log("Printing card");
                CardData cardToPrint = cardList.cards[_cardCounter];

                cardToPrint.version = cardList.version;
                _delay = DelayBetweenScreenshots;
                _cardCounter++;

                StartCoroutine(takeScreenshot(cardToPrint, Width, Height));
            } else
            {
                _delay -= Time.deltaTime;
            }
        } else
        {
            Debug.Log("Program finished running");
            Application.Quit();
            Debug.Break();
        }
        
    }

    public IEnumerator takeScreenshot(CardData cardData, int width, int height)
    {
        cardGameObject.SetCardData(cardData);

        yield return frameEnd;
        
        // TODO: Simplificar esta clase. Solo necesitamos esta parte. Averiguar porque esa diferencia de 147 pixeles
        Texture2D screenImage = new Texture2D(width, height, TextureFormat.RGBA32, true);
        //Get Image from screen
        // NOTE: Had to adjust with 147 for reasons
        screenImage.ReadPixels(new Rect(0, 0, width, height), 0, 0, false);
        screenImage.LoadRawTextureData(screenImage.GetRawTextureData());
        screenImage.Apply();

        //Convert to png
        byte[] pngBytes = screenImage.EncodeToPNG();

        //Notify functions that are subscribed to this event that picture is taken then pass in image bytes as png
        if (OnPictureTaken != null)
        {
            string fileName = cardGameObject.GetCardName();
            Debug.Log("Screenshot taken for " + fileName);
            OnPictureTaken(pngBytes, fileName);
        }
    }

    public void OnEnable()
    {
        //Un-Subscribe
        //CanvasScreenshot.OnPictureTaken -= receivePNGScreenShot;
    }

    void receivePNGScreenShot(byte[] pngArray, string fileName)
    {
        //Do Something With the Image (Save)
        string path = Application.persistentDataPath + "/"+fileName+".png";
        System.IO.File.WriteAllBytes(path, pngArray);
        Debug.Log(path);
    }

    public void LoadCardDataFromFile(string fileName)
    {
        TextAsset cardsFile = Resources.Load<TextAsset>(fileName);
        cardList = JsonUtility.FromJson<CardList>(cardsFile.text);
    }
}
